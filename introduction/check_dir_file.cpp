#include <sys/stat.h>
#include <iostream>

using namespace std;


int main()
{
    string path = "/home/l/ClionProjects/images";
    struct stat sb;
    if (stat(path.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode))
    {
        cout << path + " exists" << endl;
    } else {
        mkdir(path.c_str(), 0777);
    }
    return 0;
}
