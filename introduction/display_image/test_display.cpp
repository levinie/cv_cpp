#include <opencv2/opencv.hpp>

using namespace cv;

int main(int argc, char** argv) {
    Mat image = imread("../data/lena.jpg", IMREAD_COLOR);
    namedWindow("test_display", WINDOW_AUTOSIZE);
    imshow("test_display", image);
    waitKey(0);
    return 0;
}

